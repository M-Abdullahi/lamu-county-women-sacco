<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="text" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ asset('style/images/favicon.png') }}">
    <title> {{ str_replace('-', ' ', config('app.name')) }} - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('style/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('style/css/plugins.css' )}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('style/revolution/css/settings.css' )}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('style/revolution/css/layers.css' )}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('style/revolution/css/navigation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('style/type/icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('style/css/color/blue.css') }}">
    @yield('css')
    @stack('css')
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    {{--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
    {{--<![endif]-->--}}
</head>
<body>
<div class="content-wrapper">
    <nav class="navbar center">
        <div class="top-bar gray-wrapper">
            <div class="container flex-it">
                <div class="align-left">
                    <ul class="icon-list list-inline mb-0">
                        <li><i class="et-mail"></i> <a href="mailto:first.last@email.com" class="nocolor">info.@lamuwomensacco.or.ke</a>
                        </li>
                        <li><i class="et-old-phone"></i> +(254) 765 123 123</li>
                    </ul>
                </div>
                <!--/.align-left -->
                <div class="align-right text-right">
                    <ul class="social social-color social-s">
                        <li><a href="#"><i class="et-twitter"></i></a></li>
                        <li><a href="#"><i class="et-facebook"></i></a></li>
                        <li><a href="#"><i class="et-instagram"></i></a></li>
                    </ul>
                </div>
                <!--/.align-right -->
            </div>
            <!--/.container -->
        </div>
        <div class="container">
            <div class="flex-it">
                <div class="navbar-header visible-xs visible-sm">
                    <div class="navbar-brand"><a href="index.html"><img src="{{ asset('style/images/logo.png') }}"
                                                                        alt=""/></a></div>
                    <div class="nav-bars-wrapper">
                        <div class="nav-bars-inner">
                            <div class="nav-bars" data-toggle="collapse" data-target=".navbar-collapse"><span></span>
                            </div>
                        </div>
                        <!-- /.nav-bars-inner -->
                    </div>
                    <!-- /.nav-bars-wrapper -->
                    <div class="navbar-other">
                        <ul class="nav">
                            <li>
                                <div class="btn-group btn-search"><a href="#" data-toggle="dropdown" class="nav-link"><i
                                                class="et-magnifying-glass"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <form class="search-form">
                                            <div class="form-group mb-0">
                                                <input type="text" class="form-control" placeholder="Search something">
                                            </div>
                                            <!-- /.form-group -->
                                        </form>
                                        <!-- /.search-form -->
                                    </div>
                                    <!-- /.dropdown-menu -->
                                </div>
                                <!-- /.btn-group --></li>
                            <li>

                                <!-- /.btn-group --></li>
                        </ul>
                    </div>
                    <!-- /.navbar-other -->
                </div>
                <!-- /.nav-header -->
                <div class="navbar-collapse collapse flex-it">
                    <div class="align-left">
                        <ul class="nav navbar-nav">
                            <li class=""><a href="#!">Home </a></li>
                            <li><a href="#">Services </a></li>
                            <li><a href="#">Membership </a></li>
                            <li><a href="#">Gallery </a>
                            </li>
                        </ul>
                        <!--/.navbar-nav -->
                    </div>
                    <!--/.align-left -->
                    <div class="navbar-brand align-center hidden-xs hidden-sm"><a href="{{ url('/') }}"><img
                                    src="{{ asset('img/logo.png') }}"
                                    alt=""
                                    width="80"
                                    height="80"/ ></a>
                    </div>
                    <div class="align-right text-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#!">Downloads </a></li>
                            <li><a href="#!">Contact Us</a></li>
                            <li><a href="#!">Blog </a>

                            </li>
                        </ul>
                        <!--/.navbar-nav -->
                    </div>
                    <!--/.align-right -->
                </div>
                <!--/.nav-collapse -->
                <div class="navbar-other">
                    <ul class="nav">
                        <li>
                            <div class="btn-group btn-search"><a href="#" data-toggle="dropdown" class="nav-link"><i
                                            class="et-magnifying-glass"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <form class="search-form">
                                        <div class="form-group mb-0">
                                            <input type="text" class="form-control" placeholder="Search something">
                                        </div>
                                        <!-- /.form-group -->
                                    </form>
                                    <!-- /.search-form -->
                                </div>
                                <!-- /.dropdown-menu -->
                            </div>
                            <!-- /.btn-group --></li>
                        <li>
                            <div>
                                <a href="{{ route('login') }}" class="nav-link"> <i class="et-user"></i>Login </a>

                            </div>
                            <!-- /.btn-group --></li>
                    </ul>
                </div>
                <!-- /.navbar-other -->
            </div>
            <!--/.flex-it -->
        </div>
        <!--/.container -->
    </nav>
    <!--/.navbar -->

    <div class="rev_slider_wrapper fullwidth-container">
        <div id="slider4" class="rev_slider rs-nav-light" data-version="5.4.1">
            <ul>
                <li data-transition="fade" data-nav-color="light"><img
                            src="{{ asset('style/images/art/slider-bg10.jpg') }}" alt=""/>
                    <div class="tp-caption w-regular color-white text-center"
                         data-x="center"
                         data-y="middle"
                         data-voffset="['-35','-35','-55','-50']"
                         data-fontsize="['50','50','50','36']"
                         data-lineheight="['60','60','60','46']"
                         data-width="['1100','980','600','450']"
                         data-textAlign="['center','center','center','center']"
                         data-whitespace="['normal','normal','normal','normal']"
                         data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on"
                         data-responsive_offset="on"
                         style="z-index: 9;">LAMU County Women Sacco
                    </div>
                    <div class="tp-caption w-light color-white text-center"
                         data-x="center"
                         data-y="middle"
                         data-voffset="['35','35','55','50']"
                         data-fontsize="['28','28','28','22']"
                         data-lineheight="['38','38','38','32']"
                         data-width="['1100','980','600','400']"
                         data-textAlign="['center','center','center','center']"
                         data-whitespace="['normal','normal','normal','normal']"
                         data-frames='[{"delay":1500,"speed":1000,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on"
                         data-responsive_offset="on"
                         style="z-index: 9;">An economically empowered, financially independent women driven by our
                        products
                    </div>
                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
        <!-- /.rev_slider -->
    </div>
    <!-- /.rev_slider_wrapper -->

    <div class="wrapper light-wrapper">
        <div class="container inner">
            <h2 class="section-title text-center">Our Services</h2>
            <p class="lead text-center">We are here to serve you</p>
            <div class="space20"></div>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="feature feature-s"><span class="icon icon-color icon-m"><i
                                    class="si-seo_website-code"></i></span>
                        <h5>Pixel-Perfect Code</h5>
                        <p>Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.
                            Cras justo odio dapibus ac facilisis in egestas.</p>
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
                <div class="col-sm-6 col-md-4">
                    <div class="feature feature-s"><span class="icon icon-color icon-m"><i
                                    class="si-seo_optimization-growth-stats"></i></span>
                        <h5>SEO-Friendly</h5>
                        <p>Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.
                            Cras justo odio dapibus ac facilisis in egestas.</p>
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
                <div class="space20 visible-sm clearfix"></div>
                <div class="col-sm-6 col-md-4">
                    <div class="feature feature-s"><span class="icon icon-color icon-m"><i
                                    class="si-design_color-drop"></i></span>
                        <h5>Color Palettes</h5>
                        <p>Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.
                            Cras justo odio dapibus ac facilisis in egestas.</p>
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
                <div class="space20 hidden-xs hidden-sm clearfix"></div>
                <div class="col-sm-6 col-md-4">
                    <div class="feature feature-s"><span class="icon icon-color icon-m"><i
                                    class="si-camping_life-preserver"></i></span>
                        <h5>Free Support &amp; Updates</h5>
                        <p>Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.
                            Cras justo odio dapibus ac facilisis in egestas.</p>
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
                <div class="space20 visible-sm clearfix"></div>
                <div class="col-sm-6 col-md-4">
                    <div class="feature feature-s"><span class="icon icon-color icon-m"><i
                                    class="si-mail_read-mail"></i></span>
                        <h5>Working Contact Form</h5>
                        <p>Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.
                            Cras justo odio dapibus ac facilisis in egestas.</p>
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
                <div class="col-sm-6 col-md-4">
                    <div class="feature feature-s"><span class="icon icon-color icon-m"><i
                                    class="si-design_pen-tool"></i></span>
                        <h5>Clean &amp; Professional Design</h5>
                        <p>Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.
                            Cras justo odio dapibus ac facilisis in egestas.</p>
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.light-wrapper -->

    <div class="parallax inverse-text" data-parallax-img="style/images/art/parallax2.jpg" data-parallax-img-width="1920"
         data-parallax-img-height="1184">
        <div class="container inner pt-120 pb-120">
            <div class="row counter">
                <div class="col-sm-3 text-center">
                    <div class="icon icon-l icon-color color-dark mb-15"><i class="si-ui_users"></i></div>
                    <h3 class="value mb-15">3000</h3>
                    <p class="text-uppercase">Members</p>
                </div>
                <!--/column -->
                <div class="col-sm-3 text-center">
                    <div class="icon icon-l icon-color color-dark mb-15"><i class="si-finance_money-bag-2"></i></div>
                    <h3 class="value mb-15">3472</h3>
                    <p class="text-uppercase">Contributions made (Ksh)</p>
                </div>
                <!--/column -->
                <div class="col-sm-3 text-center">
                    <div class="icon icon-l icon-color color-dark mb-15"><i class="si-party_present-gift"></i></div>
                    <h3 class="value mb-15">2184</h3>
                    <p class="text-uppercase">Loans Given (Ksh)</p>
                </div>
                <!--/column -->
                <div class="col-sm-3 text-center">
                    <div class="icon icon-l icon-color color-dark mb-15"><i class="si-network_global-network"></i></div>
                    <h3 class="value mb-15">7</h3>
                    <p class="text-uppercase">Self help Groups</p>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /.parallax -->

    <div class="wrapper gray-wrapper">
        <div class="container inner">
            <h2 class="section-title text-center">Latest from our gallery</h2>
            <p class="lead text-center"></p>
            <div class="space15"></div>
            <div class="slick-wrapper">
                <div class="slick"
                     data-slick='{"slidesToShow": 3, "responsive": [{"breakpoint":1024,"settings":{"slidesToShow": 2}},{"breakpoint":768,"settings":{"slidesToShow": 1}}]}'>
                    <div class="item post mr-10 ml-10">
                        <figure class="overlay overlay1"><a href="#"></a><img src="style/images/art/f1.jpg" alt=""/>
                            <figcaption>
                                <h5 class="from-top">View Project</h5>
                            </figcaption>
                        </figure>
                        <div class="post-content text-center mt-20">
                            <h3 class="post-title"><a href="#">Curabitur Blandit</a></h3>
                            <div class="meta"><span class="category">Business, Works</span></div>
                            <!-- /.meta -->
                        </div>
                        <!-- /.post-content -->
                    </div>
                    <!-- /.item -->
                    <div class="item post mr-10 ml-10">
                        <figure class="overlay overlay1"><a href="#"></a><img src="style/images/art/f2.jpg" alt=""/>
                            <figcaption>
                                <h5 class="from-top mb-0">View Project</h5>
                            </figcaption>
                        </figure>
                        <div class="post-content text-center mt-20">
                            <h3 class="post-title"><a href="#">Mollis Euismod Fringilla</a></h3>
                            <div class="meta"><span class="category">News, Corporate</span></div>
                            <!-- /.meta -->
                        </div>
                        <!-- /.post-content -->
                    </div>
                    <!-- /.item -->
                    <div class="item post mr-10 ml-10">
                        <figure class="overlay overlay1"><a href="#"></a><img src="style/images/art/f3.jpg" alt=""/>
                            <figcaption>
                                <h5 class="from-top mb-0">View Project</h5>
                            </figcaption>
                        </figure>
                        <div class="post-content text-center mt-20">
                            <h3 class="post-title"><a href="#">Dapibus Quam Ligula</a></h3>
                            <div class="meta"><span class="category">Business, Works</span></div>
                            <!-- /.meta -->
                        </div>
                        <!-- /.post-content -->
                    </div>
                    <!-- /.item -->
                    <div class="item post mr-10 ml-10">
                        <figure class="overlay overlay1"><a href="#"></a><img src="style/images/art/f4.jpg" alt=""/>
                            <figcaption>
                                <h5 class="from-top mb-0">View Project</h5>
                            </figcaption>
                        </figure>
                        <div class="post-content text-center mt-20">
                            <h3 class="post-title"><a href="#">Fusce Malesuada Cursus</a></h3>
                            <div class="meta"><span class="category">News, Corporate</span></div>
                            <!-- /.meta -->
                        </div>
                        <!-- /.post-content -->
                    </div>
                    <!-- /.item -->
                    <div class="item post mr-10 ml-10">
                        <figure class="overlay overlay1"><a href="#"></a><img src="style/images/art/f5.jpg" alt=""/>
                            <figcaption>
                                <h5 class="from-top mb-0">View Project</h5>
                            </figcaption>
                        </figure>
                        <div class="post-content text-center mt-20">
                            <h3 class="post-title"><a href="#">Vehicula Euismod Mollis</a></h3>
                            <div class="meta"><span class="category">Business, Works</span></div>
                            <!-- /.meta -->
                        </div>
                        <!-- /.post-content -->
                    </div>
                    <!-- /.item -->
                    <div class="item post mr-10 ml-10">
                        <figure class="overlay overlay1"><a href="#"></a><img src="style/images/art/f6.jpg" alt=""/>
                            <figcaption>
                                <h5 class="from-top mb-0">View Project</h5>
                            </figcaption>
                        </figure>
                        <div class="post-content text-center mt-20">
                            <h3 class="post-title"><a href="#">Vehicula Justo Elit</a></h3>
                            <div class="meta"><span class="category">News, Corporate</span></div>
                            <!-- /.meta -->
                        </div>
                        <!-- /.post-content -->
                    </div>
                    <!-- /.item -->
                </div>
                <!--/.slick -->
                <div class="space20"></div>
                <div class="slick-nav-container">
                    <div class="slick-nav"></div>
                </div>
                <!--/.slick-nav-container -->
            </div>
            <!--/.slick-wrapper -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.gray-wrapper -->

    <div class="wrapper light-wrapper">
        <div class="container inner">
            <h2 class="section-title text-center">Customer Testimonials</h2>
            <div class="space10"></div>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="feature feature-l">
                        <div class="icon icon-round icon-img icon-img-m text-center"><img class="mb-10"
                                                                                          src="style/images/art/t1.jpg"
                                                                                          alt="">
                            <h6 class="mb-0">Connor Gibson</h6>
                            <p>Interface Designer</p>
                        </div>
                        <div class="box box-bg bg-white box-arrow left">
                            <blockquote class="small">
                                <p>“Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis
                                    lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla.”</p>
                            </blockquote>
                        </div>
                        <!--/.box -->
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
                <div class="space10 visible-sm visible-xs clearfix"></div>
                <div class="col-sm-12 col-md-6">
                    <div class="feature right feature-l">
                        <div class="icon icon-round icon-img icon-img-m text-center"><img class="mb-10"
                                                                                          src="style/images/art/t2.jpg"
                                                                                          alt="">
                            <h6 class="mb-0">Coriss Ambady</h6>
                            <p>Computer Engineer</p>
                        </div>
                        <!--/.feature -->
                        <div class="box box-bg bg-white box-arrow right">
                            <blockquote class="small">
                                <p>“Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac
                                    consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed
                                    consectetur.”</p>
                            </blockquote>
                        </div>
                        <!--/.box -->
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
                <div class="space10 clearfix"></div>
                <div class="col-sm-12 col-md-6">
                    <div class="feature feature-l">
                        <div class="icon icon-round icon-img icon-img-m text-center"><img class="mb-10"
                                                                                          src="style/images/art/t3.jpg"
                                                                                          alt="">
                            <h6 class="mb-0">Barclay Widerski</h6>
                            <p>Sales Manager</p>
                        </div>
                        <!--/.feature -->
                        <div class="box box-bg bg-white box-arrow left">
                            <blockquote class="small">
                                <p>“Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean
                                    lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies.”</p>
                            </blockquote>
                        </div>
                        <!--/.box -->
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
                <div class="space10 visible-sm visible-xs clearfix"></div>
                <div class="col-sm-12 col-md-6">
                    <div class="feature right feature-l">
                        <div class="icon icon-round icon-img icon-img-m text-center"><img class="mb-10"
                                                                                          src="style/images/art/t4.jpg"
                                                                                          alt="">
                            <h6 class="mb-0">Nikola Brooten</h6>
                            <p>Marketing Specialist</p>
                        </div>
                        <!--/.feature -->
                        <div class="box box-bg bg-white box-arrow right">
                            <blockquote class="small">
                                <p>“Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam id dolor
                                    id nibh ultricies vehicula ut id elit. Cum sociis natoque penatibus et magnis.”</p>
                            </blockquote>
                        </div>
                        <!--/.box -->
                    </div>
                    <!--/.feature -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.light-wrapper -->
</div>
<!-- /.content-wrapper -->
<footer class=" wrapper pattern-wrapper">
    <div class="container inner pt-60 pb-60">
        <div class="row">
            <div class="col-sm-3">
                <div class="widget"><img src="#"
                                         srcset="{{asset('style/images/logo.png')}} 1x, style/images/logo@2x.png 2x"
                                         alt="">
                    <div class="space20"></div>
                    <p>is a responsive site template with a clean and professional design. A great solution for your
                        business, portfolio, blog or any other purpose website.</p>
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->

            <div class="col-sm-2 col-sm-offset-3">
                <div class="widget">
                    <h5 class="widget-title">Need Help?</h5>
                    <ul class="list-unstyled">
                        <li><a href="#" class="nocolor">Support</a></li>
                        <li><a href="#" class="nocolor">Get Started</a></li>
                        <li><a href="#" class="nocolor">Terms of Use</a></li>
                        <li><a href="#" class="nocolor">Contact Us</a></li>
                    </ul>
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->

            <div class="col-sm-2">
                <div class="widget">
                    <h5 class="widget-title">Why Us?</h5>
                    <ul class="list-unstyled">
                        <li><a href="#" class="nocolor">Strategy</a></li>
                        <li><a href="#" class="nocolor">Service</a></li>
                        <li><a href="#" class="nocolor">Mission</a></li>
                        <li><a href="#" class="nocolor">Process</a></li>
                    </ul>
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->

            <div class="col-sm-2">
                <div class="widget">
                    <h5 class="widget-title">Learn More</h5>
                    <ul class="list-unstyled">
                        <li><a href="#" class="nocolor">About Us</a></li>
                        <li><a href="#" class="nocolor">Our Story</a></li>
                        <li><a href="#" class="nocolor">Projects</a></li>
                        <li><a href="#" class="nocolor">Features</a></li>
                    </ul>
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
    <div class="container">
        <hr class="pt-0 mb-0">
    </div>
    <!-- /.container -->
    <div class="sub-footer">
        <div class="container inner">
            <div class="cell text-left">
                <p>© 2018 {{ str_replace('-', ' ', config('app.name')) }}. All rights reserved.</p>
            </div>
            <!-- /.cell -->
            <div class="cell text-right">
                <ul class="social social-bg social-s">
                    <li><a href="#"><i class="et-twitter"></i></a></li>
                    <li><a href="#"><i class="et-facebook"></i></a></li>
                    <li><a href="#"><i class="et-pinterest"></i></a></li>
                    <li><a href="#"><i class="et-vimeo"></i></a></li>
                    <li><a href="#"><i class="et-instagram"></i></a></li>
                </ul>
            </div>
            <!-- /.cell -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.sub-footer -->
</footer>
<script type="text/javascript" src="{{ asset('style/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('style/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('style/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('style/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('style/js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('style/js/scripts.js') }}"></script>
@yield('js')
@stack('js')
</body>
</html>
