@extends('backend.master')
@section('title') Groups @stop
@section('page-header') Self help group @stop
@section('page-header-desc') create a new self-help group @stop
@section('content')

    <div class="row gutter">
        <div class="col-md-12">
            <form class="panel" action="{{ route('groups.store') }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                <div class="panel-body">
                    @include('backend.groups.form', [
                    'group' => new \App\Models\Group,
                    'user' => new \App\Models\User,
                    'action' => 'submit'
                    ])
                </div>

            </form>
        </div>
    </div>
@stop

@push('js')
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>
@endpush
