<div class="form-group has-feedback">
    <div class="row gutter">
        <div class="col-md-6">
            <label class="control-label">Group Name <span class="text-danger small">*</span></label>
            <input type="text"
                   class="form-control"
                   name="name"
                   required
                   value="{{ $group->name }}">
            @if($errors->has('name'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('name') }}</small>
            @endif
        </div>
        <div class="col-md-6">
            <label class="control-label" for="email">Group's Email</label>
            <input type="email"
                   class="form-control"
                   name="email"
                   id="email"
                   placeholder="example@lamuwonesacco.or.ke"
                   required
                   value="{{ $group->email }}">
            @if($errors->has('email'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('email') }}</small>
            @endif
        </div>
    </div>
</div>
<div class="form-group has-feedback">
    <div class="row gutter">

        <div class="col-md-6">
            <label class="control-label" for="telephone">Telephone</label>
            <input type="text"
                   class="form-control"
                   name="telephone"
                   id="telephone"
                   value="{{ $group->telephone }}"
                   required
                   placeholder="071 123 1233">
            @if($errors->has('telephone'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('telephone') }}</small>
            @endif
        </div>
        <div class="col-md-6">
            <label class="control-label" for="location">Location</label>
            <input type="text"
                   class="form-control"
                   name="location"
                   id="location"
                   required
                   placeholder="group's physical location eg lamu ...."
                   value="{{ $group->location }}" >
            @if($errors->has('location'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger"> {{ $errors->first('location') }}</small>
            @endif
        </div>
    </div>
</div>
<hr>
<hr>
<legend class="text-center text-success">Group Admin Account details</legend>
<div class="form-group has-feedback">
    <div class="row gutter">
        <div class="col-md-4">
            <label class="control-label" for="admin_name">Name <span class="text-danger small">*</span></label>
            <input type="name"
                   class="form-control"
                   name="admin_name"
                   id="admin_name"
                   value="{{ $user->name }}"
                   required
                   placeholder="Ali Mohamed">
            @if($errors->has('admin_name'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger"> {{ $errors->first('admin_name') }}</small>
            @endif
        </div>
        <div class="col-md-4">
            <label class="control-label" for="admin_email">Email <span class="text-danger small">*</span> </label>
            <input type="email"
                   class="form-control"
                   name="admin_email"
                   id="admin_email"
                   value=" {{ $user->email }}"
                   required
                   placeholder="example@lamuwomensacco.or.ke">
            @if($errors->has('admin_email'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('admin_email') }}</small>
            @endif
        </div>
        <div class="col-md-4">
            <label class="control-label" for="admin_telephone">Telephone </label>
            <input type="text"
                   class="form-control"
                   name="admin_telephone"
                   id="admin_telephone"
                   required
                   value="{{ $user->telephone }}"
                   placeholder="074 123 2344">
            @if($errors->has('admin_telephone'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('admin_telephone') }}</small>
            @endif
        </div>
    </div>
</div>
<hr>
<button type="submit" class="btn btn-success">{{ $action }}</button>
<a type="submit" href="{{ route('groups.index') }}" class="btn btn-default pull-right">Cancel</a>
<hr>
<div class="form-group">
    <span class="text-danger small">*</span> <span class="text-center-sm">Required fields</span>
</div>