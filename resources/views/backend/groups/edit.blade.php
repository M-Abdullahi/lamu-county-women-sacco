@extends('backend.master')
@section('title') Groups @stop
@section('page-header') Self help groups @stop
@section('page-header-desc') edit self-help group @stop
@section('content')

    <div class="row gutter">
        <div class="col-md-12">
            <form class="panel" action="{{ route('groups.update', $group->id) }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="panel-body">
                    @include('backend.groups.form', [
                    'group' => $group,
                    'user' => $user,
                    'action' => 'update'
                    ])
                </div>

            </form>
        </div>
    </div>
@stop

@push('js')
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>
@endpush
