@extends('backend.master')
@section('title') Groups @stop
@section('page-header') Self help group @stop
@section('page-header-desc') details about the self help group @stop
@section('content')

    <div class="row gutter">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table">
                        <table class="table table-striped table-condensed table-bordered" id="table">
                            <thead>
                            <tr>
                                <th>name</th>
                                <th>location</th>
                                <th>telephone</th>
                                <th>contact Person</th>
                                <th>Members</th>
                                <th>Contribution (Ksh)</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

@push('js')
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>
    @include('templates.datatable')

    <script>
        $(function () {
            $('#table').DataTable({
                ajax: $.fn.dataTable.pipeline({
                    url: '{!! route('groups.datatable') !!}',
                    pages: 5
                }),
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'location', name: 'location'},
                    {data: 'telephone', name: 'telephone'},
                    {data: 'user.name', name: 'user.name'},
                    {data: 'members_count', name: 'members_count'},
                    {data: 'contribution', name: 'contribution'},
                    // {data: 'roles', name: 'roles', orderable: false, searchable: false},
                    // {data: 'active', name: 'active'},
                    // {data: 'last_sign_in', name: 'last_sign_in'},
                    // {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>

@endpush
@push('css')
    <link rel="stylesheet" href="{{ asset('css/datatables/dataTables.bs.min.css') }}">
@endpush