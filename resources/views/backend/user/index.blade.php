@extends('backend.master')
@section('title') Users @stop
@section('page-header') Users @stop
@section('page-header-desc') details of system user accounts @stop
@section('content')

    <div class="row gutter">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-bordered no-margin" id="table">
                            <thead>
                            <tr>
                                <th>name</th>
                                <th>slug</th>
                                <th>email</th>
                            </tr>
                            </thead>

                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

@push('js')
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>
    @include('templates.datatable')

    <script>
        $(function () {
            $('#table').DataTable({
                ajax: $.fn.dataTable.pipeline({
                    url: '{!! route('users.datatable') !!}',
                    pages: 5
                }),
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'slug', name: 'slug'},
                    {data: 'email', name: 'email'},
                    // {data: 'roles', name: 'roles', orderable: false, searchable: false},
                    // {data: 'active', name: 'active'},
                    // {data: 'last_sign_in', name: 'last_sign_in'},
                    // {data: 'created_at', name: 'created_at'},
                    // {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>

@endpush
@push('css')
    <link rel="stylesheet" href="{{ asset('css/datatables/dataTables.bs.min.css') }}">
@endpush