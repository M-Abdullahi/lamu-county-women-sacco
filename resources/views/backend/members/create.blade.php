@extends('backend.master')
@section('title') Members @stop
@section('page-header') Members @stop
@section('page-header-desc') create new member @stop
@section('content')
    <div class="row gutter">
        <div class="col-md-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon-check2"></i><strong>{{ session()->get('message') }}</strong>
                </div>
            @endif
            <form class="panel" action="{{ route('members.store') }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                <div class="panel-body">
                    @include('backend.members.form', [
                    'member' => new \App\Models\Member,
                    'action' => 'submit'
                    ])
                </div>

            </form>
        </div>
    </div>
@stop

@push('js')
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>
@endpush
