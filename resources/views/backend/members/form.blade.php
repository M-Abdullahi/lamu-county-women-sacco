<div class="form-group has-feedback">
    <div class="row gutter">
        <div class="col-md-6">
            <label class="control-label">Full Name <span class="text-danger small">*</span></label>
            <input type="text"
                   class="form-control"
                   name="name"
                   placeholder="Ahmed Ali Hashim"
                   required
                   value="{{ $member->name }}">
            @if($errors->has('name'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('name') }}</small>
            @endif
        </div>
        <div class="col-md-6">
            <label class="control-label" for="email">Email</label>
            <input type="email"
                   class="form-control"
                   name="email"
                   id="email"
                   placeholder="example@lamuwonesacco.or.ke"
                   value="{{ $member->email }}">
            @if($errors->has('email'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('email') }}</small>
            @endif
        </div>
    </div>
</div>
<div class="form-group has-feedback">
    <div class="row gutter">

        <div class="col-md-6">
            <label class="control-label" for="telephone">Telephone <span class="text-danger small">*</span></label>
            <input type="text"
                   class="form-control"
                   name="telephone"
                   id="telephone"
                   value="{{ $member->telephone }}"
                   required
                   placeholder="0711231233">
            @if($errors->has('telephone'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('telephone') }}</small>
            @endif
        </div>
        <div class="col-md-6">
            <label class="control-label" for="nationalID">National identification number <span class="text-danger small">*</span></label>
            <input type="text"
                   class="form-control"
                   name="nationalID"
                   id="nationalID"
                   required
                   placeholder="ID number... 32324565"
                   value="{{ $member->nationalID }}" >
            @if($errors->has('nationalID'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger"> {{ $errors->first('nationalID') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="form-group has-feedback">
    <div class="row gutter">

        <div class="col-md-6">
            <label class="control-label" for="telephone">Membership Number </label>
            <input type="text"
                   class="form-control"
                   name="membershipNo"
                   id="membershipNo"
                   value="{{ $member->membershipNo }}"
                   required
                   placeholder="membership number">
            @if($errors->has('membershipNo'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('membershipNo') }}</small>
            @endif
        </div>

    </div>
</div>

<button type="submit" class="btn btn-success">{{ $action }}</button>
<a type="submit" href="{{ route('members.index') }}" class="btn btn-default pull-right">Cancel</a>
<hr>
<div class="form-group">
    <span class="text-danger small">*</span> <span class="text-center-sm">Required fields</span>
</div>