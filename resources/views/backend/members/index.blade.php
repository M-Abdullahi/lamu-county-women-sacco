@extends('backend.master')
@section('title') Members @stop
@section('page-header') Members @stop
@section('page-header-desc') details about registered members @stop
@section('content')

    <div class="row gutter">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed table-bordered no-margin" id="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>membershipNo</th>
                                <th>email</th>
                                <th>telephone</th>
                                <th>Identification No</th>
                                <th>Contribution (Ksh)</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

@push('js')
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>
    @include('templates.datatable')

    <script>
        $(function () {
            $('#table').DataTable({
                ajax: $.fn.dataTable.pipeline({
                    url: '{!! route('members.datatable') !!}',
                    pages: 5
                }),
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'membershipNo', name: 'membershipNo'},
                    {data: 'email', name: 'email'},
                    {data: 'telephone', name: 'telephone'},
                    {data: 'nationalID', name: 'nationalID', orderable: false, searchable: false},
                    {data: 'contribution', name: 'contribution'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>

@endpush
@push('css')
    <link rel="stylesheet" href="{{ asset('css/datatables/dataTables.bs.min.css') }}">
@endpush