<div class="form-group has-feedback">
    <div class="row gutter">
        <div class="col-md-4">
            <label class="control-label" for="contribution_date">Transaction date <span
                        class="text-danger small">*</span></label>
            <input type="date"
                   class="form-control"
                   required
                   name="contribution_date"
                   id="contribution_date">
            @if($errors->has('contribution_date'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('contribution_date') }}</small>
            @endif
        </div>
        <div class="col-md-4">
            <label class="control-label" for="control">Transaction Code </label>
            <input type="text"
                   class="form-control"
                   name="transaction_code"
                   id="control"
                   placeholder="Transaction code for the contribution"
                   value="{{ $contribution->transaction_code }}">
            @if($errors->has('transaction_code'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('transaction_code') }}</small>
            @endif
        </div>
        <div class="col-md-4">
            <label class="control-label" for="amount">Amount <span class="text-danger small">*</span></label>
            <input type="number"
                   class="form-control"
                   name="amount"
                   id="amount"
                   required
                   placeholder="Amount contributed in numeric eg 3400"
                   value="{{ $contribution->amount }}">
            @if($errors->has('amount'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('amount') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="form-group has-feedback">
    <div class="row gutter">
        <div class="col-md-4">
            <label class="control-label" for="member_id">Member</label>
            <select class="form-control" id="member_id" name="member_id">
                @foreach($members as $member)
                    <option value="{{ $member->id }}">{{ $member->name }}</option>
                @endforeach
            </select>
            @if($errors->has('member_id'))
                <i class="form-control-feedback"></i>
                <small class="help-block text-danger">{{ $errors->first('member_id') }}</small>
            @endif
        </div>

    </div>
</div>
<button type="submit" class="btn btn-success">{{ $action }}</button>
<a type="submit" href="{{ route('contributions.index') }}" class="btn btn-default pull-right">Cancel</a>
<hr>
<div class="form-group">
    <span class="text-danger small">*</span> <span class="text-center-sm">Required fields</span>
</div>