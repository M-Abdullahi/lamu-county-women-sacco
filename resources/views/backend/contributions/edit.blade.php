@extends('backend.master')
@section('title') Contributions @stop
@section('page-header') Contributions @stop
@section('page-header-desc') update contribution details @stop
@section('content')
    <div class="row gutter">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon-check2"></i><strong>{{ session()->get('success') }}</strong>
                </div>
            @endif
            <form class="panel" action="{{ route('contributions.update', $contribution->id) }}" method="post"
                  autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="panel-body">
                    @include('backend.contributions.form', [
                    'contribution' => $contribution,
                    'action' => 'update'
                    ])
                </div>

            </form>
        </div>
    </div>
@stop

@push('js')
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>
@endpush
