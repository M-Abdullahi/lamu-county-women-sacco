@extends('backend.master')
@section('title') Contributions @stop
@section('page-header') Contributions @stop
@section('page-header-desc') create new contribution details @stop
@section('content')
    <div class="row gutter">
        <div class="col-md-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon-check2"></i><strong>{{ session()->get('message') }}</strong>
                </div>
            @endif
            <form class="panel" action="{{ route('contributions.store') }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                <div class="panel-body">
                    @include('backend.contributions.form', [
                    'contribution' => new \App\Models\Contribution,
                    'action' => 'submit'
                    ])
                </div>

            </form>
        </div>
    </div>
@stop

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script>
        $('select').select2()
    </script>
@endpush

@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

@endpush
