@extends('backend.master')
@section('title') Contributions @stop
@section('page-header') Contributions @stop
@section('page-header-desc') details about member contributions @stop
@section('content')

    <div class="row gutter">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table">
                        <table class="table table-striped table-condensed table-bordered" id="table">
                            <thead>
                            <tr>
                                <th>Amount</th>
                                <th>Transaction Code</th>
                                <th>Date</th>
                                <th>Member name</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

@push('js')
    <script src="{{ asset('js/datatables/dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>
    @include('templates.datatable')

    <script>
        $(function () {
            $('#table').DataTable({
                ajax: $.fn.dataTable.pipeline({
                    url: '{!! route('contributions.datatable') !!}',
                    pages: 5
                }),
                columns: [
                    {data: 'amount', name: 'amount'},
                    {data: 'transaction_code', name: 'transaction_code'},
                    {data: 'contribution_date', name: 'contribution_date'},
                    {data: 'member.name', name: 'member.name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>

@endpush
@push('css')
    <link rel="stylesheet" href="{{ asset('css/datatables/dataTables.bs.min.css') }}">
@endpush