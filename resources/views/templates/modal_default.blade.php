<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Users Report Generator</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <form action="{{ route('users.export') }}" method="GET" autocomplete="off">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Registration Date:</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" id="daterange-btn" name="date">

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Account status</label>
                                <select name="active" class="form-control">
                                    <option value="1">All (Active + Inactive)</option>
                                    <option value="1">Active</option>
                                    <option value="0">In Active</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">SortBy:</label>
                                <select name="sortBy" class="form-control">
                                    <option value="name">Name</option>
                                    <option value="email">Email</option>
                                    <option value="created_at">Registration date</option>
                                </select>
                            </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>