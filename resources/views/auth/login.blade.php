<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Arise Admin Panel" />
        <meta name="keywords" content="Admin, Dashboard, Bootstrap3, Sass, transform, CSS3, HTML5, Web design, UI Design, Responsive Dashboard, Responsive Admin, Admin Theme, Best Admin UI, Bootstrap Theme, Themeforest, Bootstrap" />
        <meta name="author" content="Ramji" />
        <link rel="shortcut icon" href="img/fav.png">
        <title>{{ config('app.name')}}</title>

        <!-- Error CSS -->
        <link href="{{ asset('css/login.css') }}" rel="stylesheet" media="screen">

        <!-- Animate CSS -->
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet" media="screen">

        <!-- Ion Icons -->
        <link href="{{ asset('fonts/icomoon/icomoon.css') }}" rel="stylesheet" />
    </head>
    <body>
        <form action="{{ route('login') }}" method="post" id="wrapper" autocomplete="off">
            {{ csrf_field() }}

            <div id="box" class="animated bounceIn">
                <div id="top_header">
                    <img src="{{ asset('img/logo.png') }}" alt="Logo" width="150" height="150"/>
                    <h5>
                        Sign in to access to your<br />
                        control panel.
                    </h5>
                    @foreach($errors->all() as $error)
                        <h5> {{ $error }} </h5>
                    @endforeach
                </div>
                <div id="inputs">
                    <div class="form-block">
                        <input type="text" name="email" placeholder="Email">
                         <i class="icon-at"></i>
                    </div>

                    <div class="form-block">
                        <input type="password" name="password" placeholder="Password">
                        <i class="icon-key"></i>
                    </div>
                    <input type="submit" value="Sign In">
                </div>
                <div id="bottom" class="clearfix">
                    <div class="pull-right">
                        <label class="pull-right">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        </label>
                    </div>
                    <div class="pull-right">
                        <span class="cb-label">Remember Me</span>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>

{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
