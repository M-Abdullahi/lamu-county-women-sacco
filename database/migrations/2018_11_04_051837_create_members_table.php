<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateMembersTable.
 */
class CreateMembersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('group_id')->nullable();
            $table->string('membershipNo')->nullable();
            $table->string('name');
            $table->string('slug')->default('');
            $table->string('email')->nullable();
            $table->string('telephone')->nullable();
            $table->string('nationalID')->nullable();
            $table->softDeletes();
            $table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}
}
