<?php

use Illuminate\Database\Seeder;

class ContributionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Contribution::class, 10)->create();
    }
}
