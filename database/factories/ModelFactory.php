<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'group_id'       => factory(App\Models\Group::class)->create()->id,
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Group::class, function (Faker\Generator $faker) {
    return [
        'name'      => $faker->company,
        'location'  => $faker->streetAddress,
        'email'     => $faker->safeEmail,
        'telephone' => $faker->phoneNumber,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Member::class, function (Faker\Generator $faker) {
    return [
        'group_id'     => factory(App\Models\Group::class)->create()->id,
        'membershipNo' => $faker->randomDigit,
        'name'         => $faker->name,
        'email'        => $faker->safeEmail,
        'telephone'    => $faker->phoneNumber,
        'nationalID'   => $faker->swiftBicNumber,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Contribution::class, function (Faker\Generator $faker) {
    return [
        'member_id'         => $faker->numberBetween(1, 20),
        'amount'            => $faker->numberBetween(500, 5000),
        'contribution_date' => $faker->date(),
        'transaction_code'  => $faker->bankAccountNumber,
        'slug'              => $faker->slug,
    ];
});