<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Contracts\Repositories\GroupRepository::class, \App\Repositories\Eloquent\GroupRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\MemberRepository::class, \App\Repositories\Eloquent\MemberRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\ContributionRepository::class, \App\Repositories\Eloquent\ContributionRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\UserRepository::class, \App\Repositories\Eloquent\UserRepositoryEloquent::class);
        //:end-bindings:
    }
}
