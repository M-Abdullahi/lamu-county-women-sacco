<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ContributionRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface ContributionRepository extends RepositoryInterface
{
    public function getDataTable();
}
