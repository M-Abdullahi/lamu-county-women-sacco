<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MemberRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface MemberRepository extends RepositoryInterface
{
    public function getDataTable();
}
