<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\GroupRepository;
use App\Contracts\Repositories\UserRepository;
use App\Http\Requests\GroupCreateRequest;
use App\Http\Requests\GroupUpdateRequest;
use App\Validators\GroupValidator;

/**
 * Class GroupsController.
 *
 * @package namespace App\Http\Controllers;
 */
class GroupsController extends Controller
{
    /**
     * @var GroupRepository
     */
    protected $repository;

    /**
     * @var userRepository
     */
    protected $userRepository;

    /**
     * GroupsController constructor.
     *
     * @param GroupRepository $repository
     * @param UserRepository $userRepository
     */
    public function __construct(GroupRepository $repository, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->repository = $repository;
    }

    /**
     *  Fetch dataTable records of  resource
     * @return mixed
     */
    public function dataTable()
    {
        return $this->repository->getDataTable();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $groups = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $groups,
            ]);
        }

        return view('backend.groups.index', compact('groups'));
    }

    /**
     * Shows the form for creating a new resource
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  GroupCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     *
     */
    public function store(GroupCreateRequest $request)
    {
        $user = $this->userRepository->skipPresenter()->create([
            'name'      => $request->admin_name,
            'email'     => $request->admin_email,
            'telephone' => $request->admin_telephone,
            'password'  => bcrypt(config('sacco.default-password'))
        ]);

        $group = $this->repository->create([
            'name'     => $request->name,
            'email'    => $request->email,
            'location' => $request->location,
            'user_id'  => $user->id,
        ]);

        $response = [
            'message' => 'Group created.',
//            'data'    => $group->toArray(),
        ];

        if ($request->wantsJson()) {

            return response()->json($response);
        }

        return redirect()->back()->with('message', $response['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $group,
            ]);
        }

        return view('groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = $this->repository->find($id);

        $user = $group->user;

        return view('backend.groups.edit', compact('group', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  GroupUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     */
    public function update(GroupUpdateRequest $request, $id)
    {
        $groupResult = $this->repository->find($id);

        $userId = $groupResult->user_id;

        $group = $this->repository->update($request->all(), $id);

        $this->userRepository->update([
            'name'      => $request->admin_name,
            'email'     => $request->admin_email,
            'telephone' => $request->admin_telephone,
        ], $userId);

        $response = [
            'message' => 'Group updated.',
            'data'    => $group->toArray(),
        ];

        if ($request->wantsJson()) {

            return response()->json($response);
        }

        return redirect()->back()->with('message', $response['message']);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Group deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Group deleted.');
    }
}
