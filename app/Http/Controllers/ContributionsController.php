<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\ContributionRepository;
use App\Contracts\Repositories\MemberRepository;
use App\Http\Requests\ContributionCreateRequest;
use App\Http\Requests\ContributionUpdateRequest;
use App\Validators\ContributionValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ContributionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ContributionsController extends Controller
{
    /**
     * @var ContributionRepository
     */
    protected $repository;

    /**
     * @var MemberRepository
     */
    protected $memberRepository;

    /**
     * ContributionsController constructor.
     *
     * @param ContributionRepository $repository
     * @param MemberRepository $memberRepository
     */
    public function __construct(ContributionRepository $repository, MemberRepository $memberRepository)
    {
        $this->repository = $repository;
        $this->memberRepository = $memberRepository;
    }

    public function dataTable()
    {
        return $this->repository->getDataTable();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $contributions = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $contributions,
            ]);
        }

        return view('backend.contributions.index', compact('contributions'));
    }

    public function create()
    {
        $members = $this->memberRepository->skipPresenter()->all();

        return view('backend.contributions.create', compact('members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ContributionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function store(ContributionCreateRequest $request)
    {
        $this->repository->create($request->all());

        session()->flash('message', 'contribution created.');

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contribution = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $contribution,
            ]);
        }

        return view('contributions.show', compact('contribution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contribution = $this->repository->skipPresenter()->find($id);

        return view('contributions.edit', compact('contribution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ContributionUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ContributionUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $contribution = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Contribution updated.',
                'data'    => $contribution->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Contribution deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Contribution deleted.');
    }
}
