<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\MemberRepository;
use App\Http\Requests\MemberCreateRequest;
use App\Http\Requests\MemberUpdateRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class MembersController.
 *
 * @package namespace App\Http\Controllers;
 */
class MembersController extends Controller
{
    /**
     * @var MemberRepository
     */
    protected $repository;

    /**
     * MembersController constructor.
     *
     * @param MemberRepository $repository
     */
    public function __construct(MemberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function dataTable()
    {
        return $this->repository->getDataTable();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $members = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $members,
            ]);
        }

        return view('backend.members.index', compact('members'));
    }

    /**
     * Shows the form for creating a new resource
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  MemberCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(MemberCreateRequest $request)
    {
        $this->repository->create([
            'group_id'     => Auth::user()->group ? Auth::user()->group->id : null,
            'name'         => $request->name,
            'email'        => $request->email,
            'membershipNo' => $request->membershipNo,
            'telephone'    => $request->telephone,
            'nationalID'   => $request->nationalID,
        ]);

        session()->flash('success', 'Member created.');
        $response = [
            'message' => 'Member created.',
        ];

        if ($request->wantsJson()) {

            return response()->json($response);
        }

        return redirect()->back()->with('message', $response['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $member,
            ]);
        }

        return view('members.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = $this->repository->skipPresenter()->find($id);

        return view('backend.members.edit', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MemberUpdateRequest $request
     * @param  string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(MemberUpdateRequest $request, $id)
    {
        $this->repository->update([
            'name'         => $request->name,
            'email'        => $request->email,
            'membershipNo' => $request->membershipNo,
            'telephone'    => $request->telephone,
            'nationalID'   => $request->nationalID,
        ], $id);

        session()->flash('success', 'Member updated.');

        return redirect()->back();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Member deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Member deleted.');
    }
}
