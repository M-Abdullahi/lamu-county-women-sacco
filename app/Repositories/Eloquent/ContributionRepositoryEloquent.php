<?php

namespace App\Repositories\Eloquent;

use App\Contracts\Repositories\ContributionRepository;
use App\Models\Contribution;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Yajra\Datatables\Datatables;

/**
 * Class ContributionRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class ContributionRepositoryEloquent extends BaseRepository implements ContributionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Contribution::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Make DataTable of resource
     *
     * @return mixed
     * @throws \Exception
     */
    public function getDataTable()
    {
        $contributions = $this->model->with('member');

        return Datatables::of($contributions)
            ->addColumn('action', function ($contribution) {
                return '<div class="btn-group">
						<button data-toggle="dropdown" class="btn btn-default dropdown-toggle btn-xs" type="button">Action<span class="caret"></span></button>
						<ul role="menu" class="dropdown-menu">
							<li><a href="' . route('contributions.edit', $contribution->id) . '"><i class="icon-pencil"></i> Edit</a></li>
							<li>
							<form action="' . route('contributions.destroy', $contribution->id) . '" method="post">
						' . method_field('DELETE') . '
						' . csrf_field() . ' 
						<button type="submit"  onclick="return confirm(\'Are you sure you want to delete? \')"><i class="icon-trash"></i> Delete</button>
						</form>	</li>		
						</ul>
						</div>';
            })->make(true);
    }
}
