<?php

namespace App\Repositories\Eloquent;

use App\Contracts\Repositories\GroupRepository;
use App\Models\Group;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Yajra\Datatables\Datatables;

/**
 * Class GroupRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class GroupRepositoryEloquent extends BaseRepository implements GroupRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Group::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     *  Setup the repository presenter
     *
     * @return string
     */
//    public function presenter()
//    {
//        return GroupPresenter::class;
//    }

    public function getDataTable()
    {
        $groups = $this->model->with(['members', 'user']);

        return Datatables::of($groups)
            ->addColumn('members_count', function ($group) {
                return $group->membershipCount();
            })
            ->addColumn('contribution', function ($group) {
                return $group->contribution->sum('amount');
            })
            ->addColumn('action', function ($group) {
                return '<div class="btn-group">
						<button data-toggle="dropdown" class="btn btn-default dropdown-toggle btn-xs" type="button">Action<span class="caret"></span></button>
						<ul role="menu" class="dropdown-menu">
							<li><a href="' . route('groups.edit', $group->id) . '"><i class="icon-pencil"></i> Edit Group</a></li>
							<form action="' . route('groups.destroy', $group->id) . '" method="post">
						' . method_field('DELETE') . '
						' . csrf_field() . ' 
						<button type="submit"  onclick="return confirm(\'Are you sure you want to delete? \')"><i class="icon-trash"></i> Delete</button>
						</form>			
						<li class="divider"></li>
						<li><a href="' . route('members.index') . '"> <i class="icon-eye"></i> View members</a></li>
						<li><a href="' . route('contributions.index') . '"><i class="icon-eye"></i> View Contribution</a></li>
						<li><a href="' . route('groups.edit', $group->id) . '"> <i class="icon-eye"></i> Edit group</a></li>
							</ul>
						</div>';
            })->make(true);
    }

}
