<?php

namespace App\Repositories\Eloquent;

use App\Models\Member;
use Yajra\Datatables\Datatables;
use App\Presenters\MemberPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\MemberRepository;

/**
 * Class MemberRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class MemberRepositoryEloquent extends BaseRepository implements MemberRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Member::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     *  Setup the repository presenter
     *
     * @return string
     */
    public function presenter()
    {
        return MemberPresenter::class;
    }

    /**
     * Make DataTable of resource
     *
     * @return mixed
     * @throws \Exception
     */
    public function getDataTable()
    {
        $members = $this->model->with('contribution');

        return Datatables::of($members)
            ->editColumn('contribution', function ($group) {
                return $group->contribution->sum('amount');
            })->addColumn('action', function ($member) {
                return '<div class="btn-group">
						<button data-toggle="dropdown" class="btn btn-default dropdown-toggle btn-xs" type="button">Action<span class="caret"></span></button>
						<ul role="menu" class="dropdown-menu">
							<li><a href="' . route('members.edit', $member->id) . '"><i class="icon-pencil"></i> Edit</a></li>
							<li>
							<form action="' . route('members.destroy', $member->id) . '" method="post">
						' . method_field('DELETE') . '
						' . csrf_field() . ' 
						<button type="submit"  onclick="return confirm(\'Are you sure you want to delete? \')"><i class="icon-trash"></i> Delete</button>
						</form>	</li>		
						</ul>
						</div>';
            })->make(true);
    }
}
