<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Transformers\UserTransformer;
use Yajra\Datatables\Datatables;
use App\Presenters\UserPresenter;
use App\Contracts\Repositories\UserRepository;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class MemberRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     *  Setup the repository presenter
     *
     * @return string
     */
//    public function presenter()
//    {
//        return UserPresenter::class;
//    }

    /**
     * Make DataTable of resource
     *
     * @return mixed
     * @throws \Exception
     */
    public function getDataTable()
    {
        $members = $this->model->latest();

        return Datatables::of($members)->make(true);
    }
}
