<?php

namespace App\Transformers;

use App\Models\Member;
use League\Fractal\TransformerAbstract;

/**
 * Class MemberTransformer.
 *
 * @package namespace App\Transformers;
 */
class MemberTransformer extends TransformerAbstract
{
    /**
     * Transform the Member entity.
     *
     * @param \App\Models\Member $model
     *
     * @return array
     */
    public function transform(Member $model)
    {
        return [
            'id'           => (int)$model->id,
            'group_id'     => (int)$model->group_id,
            'name'         => (string)$model->name,
            'telephone'    => (string)$model->telephone,
            'slug'         => (string)$model->slug,
            'membershipNo' => (string)$model->membershipNo,
            'nationalID'   => (string)$model->nationalID,
            'created_at'   => $model->created_at->formatLocalized('%d %B %Y, %H:%M'),
            'updated_at'   => $model->updated_at->formatLocalized('%d %B %Y, %H:%M'),
        ];
    }
}
