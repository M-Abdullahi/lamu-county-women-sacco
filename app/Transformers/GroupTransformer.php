<?php

namespace App\Transformers;

use App\Models\Group;
use League\Fractal\TransformerAbstract;

/**
 * Class GroupTransformer.
 *
 * @package namespace App\Transformers;
 */
class GroupTransformer extends TransformerAbstract
{
    /**
     * Transform the Group entity.
     *
     * @param \App\Models\Group $model
     *
     * @return array
     */
    public function transform(Group $model)
    {
        return [
            'id'         => (int)$model->id,
            'name'       => (string)$model->name,
            'email'      => (string)$model->email,
            'telephone'  => (string)$model->telephone,
            'location'   => (string)$model->location,
            'slug'       => (string)$model->slug,
            'created_at' => $model->created_at->formatLocalized('%d %B %Y, %H:%M'),
            'updated_at' => $model->updated_at->formatLocalized('%d %B %Y, %H:%M')
        ];
    }
}
