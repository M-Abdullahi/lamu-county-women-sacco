<?php

namespace App\Transformers;

use App\Models\Contribution;
use League\Fractal\TransformerAbstract;

/**
 * Class ContributionTransformer.
 *
 * @package namespace App\Transformers;
 */
class ContributionTransformer extends TransformerAbstract
{
    /**
     * Transform the Contribution entity.
     *
     * @param \App\Models\Contribution $model
     *
     * @return array
     */
    public function transform(Contribution $model)
    {
        return [
            'id'                      => (int)$model->id,
            'member_id'               => (int)$model->member_id,
            'slug'                    => $model->slug,
            'amount'                  => (int)$model->amount,
            'contribution_date'       => $model->contribution_date,
            'mpesa_transaction_code'  => (string)$model->mpesa_transaction_code,
            'airtel_transaction_code' => (string)$model->airtel_transaction_code,
            'created_at'              => $model->created_at->formatLocalized('%d %B %Y, %H:%M'),
            'updated_at'              => $model->updated_at->formatLocalized('%d %B %Y, %H:%M'),
        ];
    }
}
