<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

/**
 * Class MemberTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * Transform the Member entity.
     *
     * @param \App\Models\User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'         => (int)$model->id,
            'name'       => (string)$model->name,
            'email'      => (string)$model->email,
            'telephone'  => (string)$model->telephone,
            'slug'       => (string)$model->slug,
            'isActive'   => (boolean)$model->isActive,
            'created_at' => $model->created_at->formatLocalized('%d %B %Y, %H:%M'),
            'updated_at' => $model->updated_at->formatLocalized('%d %B %Y, %H:%M'),
        ];
    }
}
