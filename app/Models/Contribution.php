<?php

namespace App\Models;

use Spatie\Activitylog\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\Activitylog\LogsActivityInterface;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class Contribution.
 *
 * @package namespace App\Models;
 */
class Contribution extends Model implements LogsActivityInterface
{
    use Sluggable, LogsActivity, RevisionableTrait;

    protected $revisionEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id', 'amount', 'contribution_date', 'transaction_code', 'slug'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'amount'
            ]
        ];
    }

    /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created') {
            return 'Contribution "' . $this->slug . '" was created';
        }

        if ($eventName == 'updated') {
            return 'Contribution "' . $this->slug . '" was updated';
        }

        if ($eventName == 'deleted') {
            return 'Contribution "' . $this->slug . '" was deleted';
        }

        return '';
    }

    /*
   |--------------------------------------------------------------------------
   | RELATIONSHIPS
   |--------------------------------------------------------------------------
   */

    /**
     * Contribution belongs to a member
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

}
