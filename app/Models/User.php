<?php

namespace App\Models;

use Spatie\Activitylog\LogsActivity;
use Illuminate\Notifications\Notifiable;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\Activitylog\LogsActivityInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements LogsActivityInterface
{
    use Notifiable, Sluggable, LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'telephone', 'password', 'isActive', 'slug', 'group_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'google2fa_secret'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created') {
            return 'User "' . $this->name . '" was created';
        }

        if ($eventName == 'updated') {
            return 'User "' . $this->name . '" was updated';
        }

        if ($eventName == 'deleted') {
            return 'User "' . $this->name . '" was deleted';
        }

        return '';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * A user can belong to one group
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
