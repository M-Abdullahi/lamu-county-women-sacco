<?php

namespace App\Models;

use Spatie\Activitylog\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\Activitylog\LogsActivityInterface;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class Group.
 *
 * @package namespace App\Models;
 */
class Group extends Model implements  LogsActivityInterface
{
    use Sluggable, LogsActivity, RevisionableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'telephone', 'slug', 'location'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created') {
            return 'Group "' . $this->name . '" was created';
        }

        if ($eventName == 'updated') {
            return 'Group "' . $this->name . '" was updated';
        }

        if ($eventName == 'deleted') {
            return 'Group "' . $this->name . '" was deleted';
        }

        return '';
    }

    public function membershipCount()
    {
        return $this->members()->count();
    }

    public function contributionSum()
    {
        return $this->contribution()->sum('amount');
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONSHIPS
    |--------------------------------------------------------------------------
    */
    public function members()
    {
        return $this->hasMany(Member::class);
    }

    /**
     *  Get the sum of each group contribution
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function contribution()
    {
        return $this->hasManyThrough(
            Contribution::class,
            Member::class,
            'group_id',
            'id');
    }

    /**
     * A group can have one contactPerson(user)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany(User::class);
    }
}
