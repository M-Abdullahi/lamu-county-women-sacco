<?php

namespace App\Models;

use Spatie\Activitylog\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogsActivityInterface;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class Member.
 *
 * @package namespace App\Models;
 */
class Member extends Model implements LogsActivityInterface
{
    use Sluggable, LogsActivity, SoftDeletes, RevisionableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'telephone', 'membershipNo', 'slug', 'nationalID', 'group_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $revisionEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been made.

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created') {
            return 'Member "' . $this->name . '" was created';
        }

        if ($eventName == 'updated') {
            return 'Member "' . $this->name . '" was updated';
        }

        if ($eventName == 'deleted') {
            return 'Member "' . $this->name . '" was deleted';
        }

        return '';
    }

    /**
     * @return Member|\Illuminate\Database\Query\Builder
     */
    public function memberContribution()
    {
        return $this->leftJoin('contributions', 'contributions.member_id', '=', 'member.id');
    }

    /*
   |--------------------------------------------------------------------------
   | RELATIONSHIPS
   |--------------------------------------------------------------------------
   */

    /**
     * A member can make many contributions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contribution()
    {
        return $this->hasMany(Contribution::class);
    }

    /**
     *  A member belongs to a group
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }
}
