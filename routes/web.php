<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.master');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/**
 * Backend Routes
 */

Route::group([
    'prefix' => 'intranet',
    'middleware' => 'auth'
], function () {

    Route::get('users/datatable', 'UsersController@dataTable')->name('users.datatable');
    Route::get('groups/datatable', 'GroupsController@dataTable')->name('groups.datatable');
    Route::get('members/datatable', 'MembersController@dataTable')->name('members.datatable');
    Route::get('contributions/datatable', 'ContributionsController@dataTable')->name('contributions.datatable');

    Route::resource('users', 'UsersController');
    Route::resource('members', 'MembersController');
    Route::resource('groups', 'GroupsController');
    Route::resource('contributions', 'ContributionsController');
});

