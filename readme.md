
## LAMU County Women Sacco

The Lamu County Women SACCO (Savings and Credit Cooperative Organization) is a registered sacco by the registrar of Societies with a constitution that mandates how it members operate and how it is governed. The SACCO has 10 board members drawn from Lamu County and representing the diversity within the County, The board members are led by Chairperson, Treasurer and Secretary.
<br><br>
The Sacco is owned, managed and run by its members who have a common bond, in this case Women residing in Lamu County. The SACCO began when self-help groups from Lamu County came together to form an organization that unites them but also to empower their members.
